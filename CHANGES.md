
# Changes in version 1.2, released 21st July 2022

* _Display unoccluded solution_: after solving a puzzle, once the "Solved!"
  text is displayed, the user can press any directional button
  (left/right/up/down) to hide the "Solved!" text and thus view the solution
  without occlusion.
* _Improved navigation in instructions_: the four text panels can be navigated
  using `LEFT`/`RIGHT` buttons.  The `A`-button behaves as `RIGHT` as well.
  Using the `B`-button navigates back to the main menu as does moving `RIGHT`
  from the last instruction panel.
* _Fixed misuse of the Japanese word "hashi" in the instructions_: "hashi" only
  means bridge, now we use the full "hashiwokakero" in the text, which means
  "to build bridges".
* _Remove unused functions from Arduboy2_: to save ~700 bytes of ROM the setup
  now does not use all Arduboy2 initialization methods such as those for sound
  and the Arduboy logo.
* _Bring back the controls from v1.0_: both the island-centric controls and the
  bridge-centric controls work now, allowing easy addition/removal of bridges
  whereever the cursor is.


# Changes in version 1.1, released 19th July 2022

* _Different controls_: adding and removing bridges is now island-centric, that
  is, moving the cursor on top of an island and holding down the `A`-button
  displays possible bridge additions; while holding the `A`-button one can use
  the direction button to add bridges in the respective direction.  Likewise
  holding the `B`-button removes bridges.
* _Menu navigation_: the `A`-button selects menu items and the `B`-button moves
  `B`ackwards to the previous menu.
* _Exiting a puzzle_: holding the `A`-button and `B`-button simultaneously will
  terminate a puzzle and navigate back to the start menu.

