
#include <cstdint>
#include <stdio.h>

#include <gtest/gtest.h>

#ifdef X86
#define PROGMEM
#endif

#include "testproblem.h"
#include "hashi.h"

void copy_solution_to_state(Hashi& hashi) {
	for (int bi = 0; bi < HASHI_MAX_BRIDGES; ++bi)
		hashi.state[bi] = hashi.solution[bi];

	hashi.recalculate_degrees();
}

TEST(HashiTest, Loading) {
	Hashi hashi;
	hashi.decode_from_pgm_memory(&problem[0]);
	ASSERT_EQ(8, hashi.height);
	ASSERT_EQ(12, hashi.width);
}

TEST(HashiTest, IslandCount) {
	Hashi hashi;
	hashi.decode_from_pgm_memory(&problem[0]);
	ASSERT_EQ(hashi.count_islands(), ((uint8_t) 22));
}

TEST(HashiTest, BridgeCount) {
	Hashi hashi;
	hashi.decode_from_pgm_memory(&problem[0]);
	ASSERT_EQ(hashi.count_bridges(), 26);
}

TEST(HashiTest, BridgeEnumerationDensity) {
	Hashi hashi;
	hashi.decode_from_pgm_memory(&problem[0]);

	BridgeEnumerator ee = hashi.bridges();
	for (uint8_t bi = 0; ee.next(); ++bi) {
		ASSERT_EQ(bi, ee.bi);
	}
}

TEST(HashiTest, BridgeEnumeration) {
	Hashi hashi;
	hashi.decode_from_pgm_memory(&problem[0]);

	BridgeEnumerator ee = hashi.bridges();

	ASSERT_EQ(ee.next(), true);
	ASSERT_EQ(ee.bi, ((uint8_t) 0));
	ASSERT_EQ(ee.r1, ((uint8_t) 0));
	ASSERT_EQ(ee.c1, ((uint8_t) 0));
	ASSERT_EQ(ee.r2, ((uint8_t) 0));
	ASSERT_EQ(ee.c2, ((uint8_t) 3));

	ASSERT_EQ(ee.next(), true);
	ASSERT_EQ(ee.bi, ((uint8_t) 1));
	ASSERT_EQ(ee.r1, ((uint8_t) 0));
	ASSERT_EQ(ee.c1, ((uint8_t) 0));
	ASSERT_EQ(ee.r2, ((uint8_t) 6));
	ASSERT_EQ(ee.c2, ((uint8_t) 0));
}

TEST(HashiTest, BridgeConflicts) {
	Hashi hashi;
	hashi.decode_from_pgm_memory(&problem[0]);

	BridgeEnumerator ee_9 = hashi.bridges();
	while (ee_9.next() && ee_9.bi != 9)
		continue;

	BridgeEnumerator ee = hashi.bridges();
	while (ee.next()) {
		if (ee.bi == 9)
			continue;

		if (ee.bi == 16 || ee.bi == 18) {
			ASSERT_EQ(ee.intersects_with(ee_9), true);
			ASSERT_EQ(ee_9.intersects_with(ee), true);
		} else {
			ASSERT_EQ(ee.intersects_with(ee_9), false);
			ASSERT_EQ(ee_9.intersects_with(ee), false);
		}
	}
}

TEST(HashiTest, DisconnectedSolution) {
	Hashi hashi;
	hashi.decode_from_pgm_memory(&problem3x3_disconnected[0]);

	copy_solution_to_state(hashi);
	ASSERT_EQ(hashi.is_solution_connected(), false);
}
TEST(HashiTest, ConnectedSolution) {
	Hashi hashi;
	hashi.decode_from_pgm_memory(&problem3x3_connected[0]);

	copy_solution_to_state(hashi);
	ASSERT_EQ(hashi.is_solution_connected(), true);
}
TEST(HashiTest, ConnectedSolutionLarge) {
	Hashi hashi;
	hashi.decode_from_pgm_memory(&problem[0]);

	copy_solution_to_state(hashi);
	ASSERT_EQ(hashi.is_solution_connected(), true);
}

int main(int argc, char** argv) {
	testing::InitGoogleTest(&argc, argv);
	testing::internal::CaptureStdout();

	int res = RUN_ALL_TESTS();

	std::cout << "Stdout:";
	std::string output = testing::internal::GetCapturedStdout();
	std::cout << output;

	return res;
}


