
# Running Unit Tests

There are unit tests available for developing the Hashi game.

Running the tests on a host system requires the Google test libraries installed
(`apt-get install googletest`).  Then run:

`cd tests.x86 && make check`


# Creating a Release Build

The code should compile with the default settings of the Arduino IDE, in
particular the default choice of optimizing for small code size (`-Os`).

