
# Hashi - Japanese Hashiwokakero puzzle game for the Arduboy

__Play it online in the Arduboy emulator:__
[click here](https://felipemanga.github.io/ProjectABE/?url=https://gitlab.com/nowozin/hashiarduboy/-/raw/main/hashiArduboy/release/v1.1/hashi.v1.1.hex)


## Game description

[Hashiwokakero](https://en.wikipedia.org/wiki/Hashiwokakero) is a Japanese
number puzzle which can be played on paper.  Given a set of _islands_ on a 2D
grid, each island having a number between 1 and 8, the goal is to build bridges
connecting all islands.  There are a number of restrictions on how these
bridges can be build:

- Bridges can only be drawn horizontally or vertically;
- Bridges are not allowed to cross each other;
- One can draw one or two bridges along each direction (single or double
  bridge);
- The total number of bridges leaving an island need to match the number
  assigned to that island.


## Arduboy-specific Hashiwokakero

The game includes 144 puzzles created by [Vegard Hanssen](https://menneske.no/):
there are puzzles for three sizes (8-by-8, 12-by-8, and 16-by-8), and for each
size there are four different difficulty levels ("easy", "medium", "hard", and
"expert"), each having 12 puzzles.


# Credits

- Puzzles: [created by Vegard Hanssen](https://menneske.no/).  There are more
  Hashi puzzles here: [Hashiwokakero puzzles](https://menneske.no/hashi/).
- Code: Sebastian Nowozin


# Contact and Bugs

If you find any bugs please file an issue in the GitLab repository, post to the
[Hashi Arduboy community thread](https://community.arduboy.com/) or reach out
via email (`nowozin@gmail.com`).


