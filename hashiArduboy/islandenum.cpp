#include "islandenum.h"
#include "hashi.h"

#include <assert.h>


// Increment (r1,c1) coordinates.
// Return true if end is reached.
bool IslandEnumerator::increment(void) {
	col += 1;
	if (col >= hashi->width) {
		row += 1;
		col = 0;
	}
	return (row >= hashi->height);
}

// Return true if has found a new island
bool IslandEnumerator::next(void) {
	assert(state != ISLAND_END_REACHED);

	// Possible state transitions:
	//
	//                     .--------------------.
	//                     |                    |
	//                     v                    |
	//         .---- ISLAND_NEW_CELL ---> ISLAND_FOUND_ISLAND (emit)
	//         |
	//         v
	// ISLAND_END_REACHED
	while (true) {
		switch (state) {
		case ISLAND_NEW_CELL:
			while (!hashi->has_island_at(row, col)) {
				if (increment()) {
					state = ISLAND_END_REACHED;
					return false;
				}
			}
			state = ISLAND_FOUND_ISLAND;
			return true;
		case ISLAND_FOUND_ISLAND:
			index += 1;
			state = ISLAND_NEW_CELL;
			break;
		}

		// Found neither
		if (increment()) {
			state = ISLAND_END_REACHED;
			return false;
		}
	}
}

void IslandEnumerator::reset(void) {
	index = 0;
	row = 0;
	col = 0;
	state = ISLAND_NEW_CELL;
}

IslandEnumerator::IslandEnumerator(const Hashi* hashi)
	: hashi(hashi)
{
	reset();
}


