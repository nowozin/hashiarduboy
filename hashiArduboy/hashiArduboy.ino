#include <assert.h>
#include <Arduboy2.h>

#include "hashi.h"
#include "menu.h"

Hashi hashi;

Arduboy2 arduboy;

enum GameState {
  START_SCREEN = 0,
  START_SCREEN_SIZE = 1,
  START_SCREEN_DIFF = 2,
  START_SCREEN_LEVELSELECT = 3,
  SHOW_RULES = 4,
  SHOW_CREDITS = 5,
  IN_GAME = 6,
  GAME_SOLVED = 7,
};
GameState gamestate = START_SCREEN;

#include "puzzles/problems_8x8_L1.h"
#include "puzzles/problems_8x8_L2.h"
#include "puzzles/problems_8x8_L3.h"
#include "puzzles/problems_8x8_L4.h"
const uint8_t** const PROGMEM problems_8x8[] = {
  problems_8x8_L1,
  problems_8x8_L2,
  problems_8x8_L3,
  problems_8x8_L4,
};

#include "puzzles/problems_12x8_L1.h"
#include "puzzles/problems_12x8_L2.h"
#include "puzzles/problems_12x8_L3.h"
#include "puzzles/problems_12x8_L4.h"
const uint8_t** const PROGMEM problems_12x8[] = {
  problems_12x8_L1,
  problems_12x8_L2,
  problems_12x8_L3,
  problems_12x8_L4,
};

#include "puzzles/problems_16x8_L1.h"
#include "puzzles/problems_16x8_L2.h"
#include "puzzles/problems_16x8_L3.h"
#include "puzzles/problems_16x8_L4.h"
const uint8_t** const PROGMEM problems_16x8[] = {
  problems_16x8_L1,
  problems_16x8_L2,
  problems_16x8_L3,
  problems_16x8_L4,
};

#include "testproblem.h"
//#include "hashilogo.h"

const char* menu_main_text[] = {
  "play",
  "instructions",
  "credits",
};
Menu menu_main = {
  24,
  28,
  3,
  0,
  menu_main_text,
};

const char* menu_size_text[] = {
  "8x8",
  "12x8",
  "16x8",
};
Menu menu_size = {
  24,
  28,
  3,
  0,
  menu_size_text,
};

const char* menu_difficulty_text[] = {
  "easy",
  "medium",
  "hard",
  "expert",
};
Menu menu_difficulty = {
  24,
  25,
  4,
  1,
  menu_difficulty_text,
};

enum ProblemDifficulty {
  DIFFICULTY_EASY = 0,
  DIFFICULTY_MEDIUM = 1,
  DIFFICULTY_HARD = 2,
  DIFFICULTY_EXPERT = 3,
};
enum ProblemSize {
  SIZE_8x8 = 0,
  SIZE_12x8 = 1,
  SIZE_16x8 = 2,
};
ProblemDifficulty difficulty = DIFFICULTY_EASY;
ProblemSize problem_size = SIZE_8x8;

#include "rules.h"
uint8_t rule_sheet = 0;

#define pos_t uint8_t

#define CELL_DRAW_WIDTH 8
#define CELL_DRAW_HEIGHT 8
#include "assets.h"
const unsigned char* digits[] = {
  digit0, digit1, digit2, digit3, digit4,
  digit5, digit6, digit7, digit8, digit9,
};
const unsigned char* digits_inverted[] = {
  digit0inv, digit1inv, digit2inv, digit3inv, digit4inv,
  digit5inv, digit6inv, digit7inv, digit8inv, digit9inv,
};

uint8_t drawBaseX(void) {
  // Hashi can be 8, 12, or 16 cells wide.  Each cell is 8 pixels wide.
  // We use the following starting points:
  //    8 cells (total width is 8*8 = 64 pixels): 32
  //   12 cells (total width is 12*8 = 96 pixels): 16
  //   16 cells (total width is 16*8 = 128 pixels): 0
  switch (hashi.width) {
    case 8:
      return 32;
    case 12:
      return 16;
    case 16:
      // FALLTHROUGH
    default:
      return 0;
  }
}
uint8_t drawBaseY(void) {
  return 0;
}

// Drawing functions
void drawBlock(pos_t row, pos_t col, const unsigned char* block) {
  int x = drawBaseX() + col*CELL_DRAW_WIDTH;
  int y = drawBaseY() + row*CELL_DRAW_HEIGHT;
  Sprites::drawOverwrite(x, y, block, 0);
}
void drawIsland(const IslandEnumerator& island) {
  uint8_t row = island.row;
  uint8_t col = island.col;
  if (!hashi.has_island_at(row, col))
    return;

  uint8_t value = hashi.board(row, col);
  assert(value <= 9);

  // Invert if degree satisfied
  uint8_t degree = hashi.current_degree(row, col);

  const unsigned char* digit = NULL;
  if (degree == value) {
    digit = digits_inverted[value];
  } else {
    digit = digits[value];
  }
  drawBlock(row, col, digit);
}
void drawBridgeForce(
  uint8_t r1,
  uint8_t c1,
  uint8_t r2,
  uint8_t c2,
  uint8_t value)
{
  assert(value <= 2);

  bool is_horizontal = (r1 == r2);

  const unsigned char* bar = NULL;
  if (is_horizontal) {
    for (uint8_t col = c1+1; col < c2; ++col) {
      bar = emptyblock;
      if (value > 0) {
        bar = (value == 1) ? hbar1 : hbar2;
        if (col == (c2-1))
          bar = (value == 1) ? hbar1end : hbar2end;
      }

      drawBlock(r1, col, bar);
    }
  } else {
    for (uint8_t row = r1+1; row < r2; ++row) {
      bar = emptyblock;
      if (value > 0) {
        bar = (value == 1) ? vbar1 : vbar2;
        if (row == (r2-1))
          bar = (value == 1) ? vbar1end : vbar2end;
      }

      drawBlock(row, c1, bar);
    }
  }
}
void drawBridge(
  uint8_t r1,
  uint8_t c1,
  uint8_t r2,
  uint8_t c2,
  uint8_t value)
{
  if (value == 0)
    return;

  drawBridgeForce(r1, c1, r2, c2, value);
}
void drawCurrentBridge(const BridgeEnumerator& ee, uint8_t value) {
  drawBridge(ee.r1, ee.c1, ee.r2, ee.c2, value);
}

uint8_t draw_comp_index = 0;

void drawSolution(const solution_t* sol) {
  draw_comp_index += 1;
  bool solution_degree = hashi.is_solution_degree();

  BridgeEnumerator ee = hashi.bridges();
  if (solution_degree == false) {
    for (uint8_t bi = 0; ee.next(); ++bi)
      drawCurrentBridge(ee, sol[bi]);
  } else {
    solution_t comp[HASHI_MAX_BRIDGES];
    uint8_t comp_count = hashi.label_connected_components(comp);
    if (comp_count == 1) {
      // Only one component, draw as usual
      for (uint8_t bi = 0; ee.next(); ++bi)
        drawCurrentBridge(ee, sol[bi]);
      return;
    }

    // Multiple components, only draw current component index
    uint8_t index_to_draw = (draw_comp_index % comp_count) + 1;
    for (uint8_t bi = 0; ee.next(); ++bi) {
      if (index_to_draw == comp[bi])
        drawCurrentBridge(ee, sol[bi]);
    }
  }
}
void drawHashi(void) {
  IslandEnumerator island = hashi.islands();
  while (island.next()) {
    drawIsland(island);
  }
}

uint8_t cursor_color = WHITE;
#define CURSOR_DEFAULT_ROW  0
#define CURSOR_DEFAULT_COL  0
int cursor_row = CURSOR_DEFAULT_ROW;
int cursor_col = CURSOR_DEFAULT_COL;

void drawCursor(void) {
  int x = drawBaseX() + cursor_col*CELL_DRAW_WIDTH;
  int y = drawBaseY() + cursor_row*CELL_DRAW_HEIGHT;

  arduboy.drawRect(
    x-1, y-1,
    CELL_DRAW_WIDTH+1, CELL_DRAW_HEIGHT+1,
    cursor_color);
}

void updateCursor(void) {
  // Make cursor flicker
  //cursor_color = (cursor_color == WHITE) ? BLACK : WHITE;

  // Update cursor
  if (arduboy.justPressed(UP_BUTTON) && cursor_row > 0)
    cursor_row -= 1;
  if (arduboy.justPressed(DOWN_BUTTON) && cursor_row < (hashi.height-1))
    cursor_row += 1;

  if (arduboy.justPressed(LEFT_BUTTON) && cursor_col > 0)
    cursor_col -= 1;
  if (arduboy.justPressed(RIGHT_BUTTON) && cursor_col < (hashi.width-1))
    cursor_col += 1;
}

// Move solution value in the given direction with wrap-around.
// This function is only to be used for single bridge cells.
uint8_t multiplicityAction(uint8_t sol_value, bool direction_up) {
  if (direction_up) {
    sol_value = (sol_value < 2) ? (sol_value+1) : 0;
  } else {
    sol_value = (sol_value > 0) ? (sol_value-1) : 2;
  }
  return sol_value;
}

void multiplicityActionTwoBridges(
  uint8_t& sol_h, uint8_t& sol_v, bool direction_up)
{
  // Sequentially move, first sol_h, then sol_v.
  if (direction_up) {
    // Upwards
    if (sol_h == 0 && sol_v == 0) {
      sol_h = 1;
      sol_v = 0;
    } else if (sol_h > 0 && sol_v == 0) {
      sol_h += 1;
      if (sol_h == 3) {
        sol_h = 0;
        sol_v = 1;
      }
    } else if (sol_h == 0 && sol_v > 0) {
      sol_v += 1;
      if (sol_v == 3) {
        sol_h = sol_v = 0;
      }
    }
  } else {
    if (sol_h == 0 && sol_v == 0) {
      sol_h = 0;
      sol_v = 2;
    } else if (sol_h > 0 && sol_v == 0) {
      sol_h -= 1;
    } else if (sol_h == 0 && sol_v == 1) {
      sol_h = 2;
      sol_v = 0;
    } else if (sol_h == 0 && sol_v == 2) {
      sol_h = 0;
      sol_v = 1;
    }
  }
}

// Remove all bridges conflicting with bridge bi.
// Return the number of conflicting bridges removed from state.
uint8_t removeBridgesConflictingWith(uint8_t bi) {
  uint8_t removed_count = 0;

  // Make two passes over all bridges:
  //   1. First pass: find bi bridge coordinates.
  //   2. Second pass: identify all conflicts.
  BridgeEnumerator ee_bi = hashi.bridges();
  while (ee_bi.next()) {
    if (ee_bi.bi == bi)
      break;
  }
  assert(ee_bi.bi == bi);

  BridgeEnumerator ee_cf = hashi.bridges();
  while (ee_cf.next()) {
    // Skip the bridge we are seeking conflicts for
    if (ee_cf.bi == bi)
      continue;

    // For this bridge, check if it intersects
    if (ee_cf.intersects_with(ee_bi) == false)
      continue;

    // We have an intersection, remove conflicting bridge
    hashi.state[ee_cf.bi] = 0;
    removed_count += 1;
  }
  return removed_count;
}

// direction_up == true: cycle towards adding multiplicities;
// direction_up == false: cycle backwards.
void bridgeAction(bool direction_up) {
  int16_t bi_h = hashi.find_bridge_at(cursor_row, cursor_col, true);
  int16_t bi_v = hashi.find_bridge_at(cursor_row, cursor_col, false);
  if (bi_h == -1 && bi_v == -1) {
    // No bridge, nothing to do here
    return;
  }

  if (bi_h != -1 && bi_v == -1) {
    // Only horizontal bridge
    hashi.state[bi_h] = multiplicityAction(hashi.state[bi_h], direction_up);
  } else if (bi_h == -1 && bi_v != -1) {
    // Only vertical bridge
    hashi.state[bi_v] = multiplicityAction(hashi.state[bi_v], direction_up);
  } else {
    // Both horizontal and vertical bridge
    multiplicityActionTwoBridges(
      hashi.state[bi_h], hashi.state[bi_v], direction_up);
  }

  // Remove all bridges in conflict with bi_h, bi_v, if active
  if (bi_h != -1 && hashi.state[bi_h] > 0)
    removeBridgesConflictingWith(bi_h);
  if (bi_v != -1 && hashi.state[bi_v] > 0)
    removeBridgesConflictingWith(bi_v);
}

uint8_t count_levels(const uint8_t** problems) {
  uint8_t levels_available = 0;
  const uint8_t* problem = NULL;
  for ( ; true ; ++levels_available) {
    problem = (const uint8_t*) pgm_read_word(&(problems[levels_available]));
    if (problem == NULL)
      break;
  }
  return levels_available;
}

const uint8_t** load_problems(
  ProblemSize size,
  ProblemDifficulty difficulty)
{
  // Select problem set by size
  const uint8_t*** problem_size_sets = NULL;
  switch (size) {
    case SIZE_8x8:
      problem_size_sets = problems_8x8;
      break;
    case SIZE_12x8:
      problem_size_sets = problems_12x8;
      break;
    case SIZE_16x8:
      problem_size_sets = problems_16x8;
      break;
  }
  assert(problem_size_sets != NULL);

  // Load problem set by level
  assert(difficulty >= DIFFICULTY_EASY && difficulty <= DIFFICULTY_EXPERT);
  const uint8_t** problems = (const uint8_t**) pgm_read_word(
    &(problem_size_sets[difficulty]));

  return problems;
}

void load_puzzle(
  ProblemSize size,
  ProblemDifficulty difficulty,
  uint8_t level)
{
  const uint8_t** problems = load_problems(size, difficulty);

  // Count levels
  uint8_t levels_available = count_levels(problems);
  assert(level < levels_available);

  const uint8_t* problem = (const uint8_t*) pgm_read_word(&(problems[level]));
  hashi.decode_from_pgm_memory(problem);
}

uint8_t level_selected = 0;

void handleLevelSelect(void) {
  drawHashi();

  uint8_t old_level_selected = level_selected;
  const uint8_t** problems = load_problems(problem_size, difficulty);
  uint8_t levels_available = count_levels(problems);

  int base_x = 36;
  int x = 3;
  if (level_selected <= 8)
    x += 8;

  arduboy.fillRect(base_x, 50, 44, 11, BLACK);
  arduboy.setCursor(base_x + x, 52);
  arduboy.print(level_selected + 1);
  arduboy.setCursor(base_x + 18, 52);
  arduboy.print("/");
  arduboy.setCursor(base_x + 28, 52);
  arduboy.print(levels_available);
  arduboy.drawRect(base_x, 50, 44, 11, WHITE);

  if (arduboy.justPressed(RIGHT_BUTTON)) {
    if (level_selected < (levels_available-1))
      level_selected += 1;
  } else if (arduboy.justPressed(LEFT_BUTTON)) {
    if (level_selected > 0)
      level_selected -= 1;
  }
  if (level_selected != old_level_selected) {
    load_puzzle(problem_size, difficulty, level_selected);
    return;
  }

  if (arduboy.justPressed(A_BUTTON)) {
    gamestate = IN_GAME;
  } else if (arduboy.justPressed(B_BUTTON)) {
    gamestate = START_SCREEN_DIFF;
  }
}

void handleStartScreen(void) {
  Sprites::drawOverwrite(0, 0, hashilogo_cropped, 0);   // frame=0

  if (gamestate == START_SCREEN) {
    if (handleMenu(&arduboy, &menu_main) == SELECTION_MADE) {
      switch (menu_main.current_selection) {
        case 0:
          gamestate = START_SCREEN_SIZE;
          break;
        case 1:
          gamestate = SHOW_RULES;
          rule_sheet = 0;
          break;
        case 2:
          gamestate = SHOW_CREDITS;
          break;
      }
    }
  } else if (gamestate == START_SCREEN_SIZE) {
    MenuResult res = handleMenu(&arduboy, &menu_size);
    if (res == BACK) {
      gamestate = START_SCREEN;
    } else if (res == SELECTION_MADE) {
      problem_size = (ProblemSize) menu_size.current_selection;
      gamestate = START_SCREEN_DIFF;
    }
  } else if (gamestate == START_SCREEN_DIFF) {
    MenuResult res = handleMenu(&arduboy, &menu_difficulty);
    if (res == BACK) {
      gamestate = START_SCREEN_SIZE;
    } else if (res == SELECTION_MADE) {
      difficulty = (ProblemDifficulty) menu_difficulty.current_selection;

      // Setup for level selection
      level_selected = 0;
      load_puzzle(problem_size, difficulty, level_selected);
      gamestate = START_SCREEN_LEVELSELECT;
    }
  }
}

void printRules(const char** rules) {
  char buffer[24];

  for (uint8_t y = 0; true; ++y) {
    const char* str = (const char*) pgm_read_word(&(rules[y]));
    if (str == NULL)
      return;

    // Move from PROGMEM to RAM
    strcpy_P(buffer, str);
    arduboy.setCursor(0, 10*y);
    arduboy.print(buffer);
  }
}

void handleRules(void) {
  const char** current_rules = (const char**) pgm_read_word(&(rules[rule_sheet]));
  printRules(current_rules);
  if (arduboy.justPressed(LEFT_BUTTON) && rule_sheet > 0) {
    rule_sheet -= 1;
  } else if (arduboy.justPressed(RIGHT_BUTTON) || arduboy.justPressed(A_BUTTON)) {
    rule_sheet += 1;

    current_rules = (const char**) pgm_read_word(&(rules[rule_sheet]));
    if (current_rules == NULL) {
      goto rules_back_to_main;
    }
  } else if (arduboy.justPressed(B_BUTTON)) {
    goto rules_back_to_main;
  }
  return;

rules_back_to_main:
  gamestate = START_SCREEN;
  rule_sheet = 0;
}

bool justPressedAB(void) {
  return (arduboy.justPressed(A_BUTTON) || arduboy.justPressed(B_BUTTON));
}

void handleCredits(void) {
  Sprites::drawOverwrite(0, 0, hashilogo_cropped, 0);   // frame=0
  arduboy.setCursor(0, 32);
  arduboy.print("Code: S. Nowozin");
  arduboy.setCursor(0, 32+10);
  arduboy.print("Puzzles: menneske.no");

  if (justPressedAB()) {
    gamestate = START_SCREEN;
  }
}

uint8_t solved_frame = 0;
bool display_solved_text = true;

void handleGameSolved(void) {
  drawHashi();
  drawSolution(hashi.state);

  solved_frame += 1;
  solved_frame &= 0x0f;  // 0000.1111

  if (display_solved_text) {
    int base_x = 40;
    arduboy.fillRect(base_x, 50, 45, 11, BLACK);
    arduboy.setCursor(base_x + 3, 52);
    // Blink "Solved!"
    if (solved_frame & 0x0f > 0x08) {
      arduboy.print("Solved!");
    }
    arduboy.drawRect(base_x, 50, 45, 11, WHITE);
  }
  if (arduboy.justPressed(UP_BUTTON)
    || arduboy.justPressed(RIGHT_BUTTON)
    || arduboy.justPressed(DOWN_BUTTON)
    || arduboy.justPressed(LEFT_BUTTON))
  {
    display_solved_text = false;
  }

  if (justPressedAB()) {
    gamestate = START_SCREEN;
    display_solved_text = true;
  }
}

uint8_t gameframe = 0;

void handleGame(void) {
  gameframe += 1;

  drawHashi();
  drawSolution(hashi.state);
  bool pressed_A = arduboy.pressed(A_BUTTON);
  bool pressed_B = arduboy.pressed(B_BUTTON);
  if (pressed_A && pressed_B) {
    // Abort, back to main menu
    gamestate = START_SCREEN;
    return;
  }

  if (pressed_A == false && pressed_B == false) {
    // Only allow movement of cursor when no button is pressed
    updateCursor();
  }

  bool recompute_needed = false;
  bool has_island_at_cursor = hashi.has_island_at(cursor_row, cursor_col);

  if (has_island_at_cursor) {
    // Show options
    if (pressed_A || pressed_B) {
      // Find all possible bridges
      for (uint8_t bdir = BDIR_UP; bdir <= BDIR_LEFT; ++bdir) {
        int16_t bi = hashi.find_bridge_at_island(
          cursor_row, cursor_col, (BridgeDirection) bdir);
        if (bi < 0)
          continue;
  
        BridgeEnumerator ee = hashi.bridge(bi);
        uint8_t sol_bi = hashi.state[bi];
        uint8_t sol_show = sol_bi;
        if (pressed_A && sol_bi < 2)
          sol_show += 1;
        if (pressed_B && sol_bi > 0)
          sol_show -= 1;
  
        if (gameframe % 2 == 0)
          sol_show = sol_bi;
  
        if (sol_show != sol_bi) {
          drawBridgeForce(ee.r1, ee.c1, ee.r2, ee.c2, sol_show);
        }
      }
    }
  
    // Draw cursor afterwards to restore rectangle
    drawCursor();
  
    // Check if any change to solution is needed
    bool just_released_up = arduboy.justReleased(UP_BUTTON);
    bool just_released_right = arduboy.justReleased(RIGHT_BUTTON);
    bool just_released_down = arduboy.justReleased(DOWN_BUTTON);
    bool just_released_left = arduboy.justReleased(LEFT_BUTTON);
    bool just_released_direction =
      just_released_up | just_released_right |
      just_released_down | just_released_left;
    BridgeDirection bdir = BDIR_UP;
    if (just_released_up)
      bdir = BDIR_UP;
    if (just_released_right)
      bdir = BDIR_RIGHT;
    if (just_released_down)
      bdir = BDIR_DOWN;
    if (just_released_left)
      bdir = BDIR_LEFT;
  
    if (just_released_direction && (pressed_A || pressed_B)) {
      int16_t bi = hashi.find_bridge_at_island(cursor_row, cursor_col, bdir);
      if (bi >= 0) {
        BridgeEnumerator ee = hashi.bridge(bi);
        uint8_t sol_bi = hashi.state[bi];
        uint8_t sol_new = sol_bi;
        if (pressed_A && sol_bi < 2)
          sol_new += 1;
        if (pressed_B && sol_bi > 0)
          sol_new -= 1;
  
        if (sol_new > 0)
          removeBridgesConflictingWith(bi);
  
        hashi.state[bi] = sol_new;
        recompute_needed = true;
      }
    }
  } else {
    drawCursor();

    // v1.0 mode on non-island locations
    if (arduboy.justReleased(A_BUTTON)) {
      bridgeAction(true);
      recompute_needed = true;
    }
    if (arduboy.justReleased(B_BUTTON)) {
      bridgeAction(false);
      recompute_needed = true;
    }
  }

  if (recompute_needed) {
    hashi.recalculate_degrees();
    if (hashi.is_solution_degree() && hashi.is_solution_connected()) {
      gamestate = GAME_SOLVED;
    }
  }
}

void reset(void) {
  //hashi.decode_from_pgm_memory(hashilogo);
  cursor_row = CURSOR_DEFAULT_ROW;
  cursor_col = CURSOR_DEFAULT_COL;
}

void setup() {
#if 0
  arduboy.begin();
#endif
  arduboy.boot();
  arduboy.display();
 
  // flashlight() or safeMode() should always be included to provide
  // a method of recovering from the bootloader "magic key" problem.
  //arduboy.flashlight();
  arduboy.safeMode();
  
  // Wait for all buttons to be released, in case a pressed one might
  // cause problems by being acted upon when the actual sketch code
  // starts. If neither systemButtons() nor bootLogo() is kept, this
  // function isn't required.
  arduboy.waitNoButtons();

  arduboy.setFrameRate(10);
  arduboy.initRandomSeed();

  reset();
}

void loop() {
  if (!(arduboy.nextFrame()))
    return;

  arduboy.pollButtons();
  arduboy.clear();

  // gamestate
  switch (gamestate) {
    case START_SCREEN:
    case START_SCREEN_SIZE:
    case START_SCREEN_DIFF:
      handleStartScreen();
      break;
    case START_SCREEN_LEVELSELECT:
      handleLevelSelect();
      break;
    case SHOW_RULES:
      handleRules();
      break;
    case SHOW_CREDITS:
      handleCredits();
      break;
    case IN_GAME:
      handleGame();
      break;
    case GAME_SOLVED:
      handleGameSolved();
      break;
  }

  arduboy.display();
}
