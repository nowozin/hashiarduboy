
#include "menu.h"

MenuResult handleMenu(Arduboy2* arduboy, Menu* menu) {
	// Draw text options on screen
	for (uint8_t item = 0; item < menu->item_count; ++item) {
		int x = menu->draw_pos_x + 10;
		int y = menu->draw_pos_y + item*10;
		arduboy->setCursor(x, y);
		arduboy->print(menu->item_text[item]);
	}

	// Handle button presses
	if (arduboy->justPressed(UP_BUTTON)) {
		if (menu->current_selection == 0) {
			menu->current_selection = menu->item_count - 1;
		} else {
			menu->current_selection -= 1;
		}
	}
	if (arduboy->justPressed(DOWN_BUTTON)) {
		menu->current_selection += 1;
		menu->current_selection %= menu->item_count;
	}

	// Draw cursor for current selection
	arduboy->setCursor(
		menu->draw_pos_x,
		menu->draw_pos_y + menu->current_selection*10);
	arduboy->print(">");

	// Selection made: return true
	if (arduboy->justPressed(A_BUTTON)) {
		return SELECTION_MADE;
	}
	if (arduboy->justPressed(B_BUTTON)) {
		return BACK;
	}

	return IN_MENU;
}

