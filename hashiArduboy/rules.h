// 21x6
                              // .........x.........x.XXX
const char rules1_0[] PROGMEM = "'Hashiwokakero' means";
const char rules1_1[] PROGMEM = "'building bridges'.";
const char rules1_2[] PROGMEM = "The goal is to add";
const char rules1_3[] PROGMEM = "bridges to connect";
const char rules1_4[] PROGMEM = "all 'islands' with";
const char rules1_5[] PROGMEM = "each other.     (1/4)";
const char *const rules1[] PROGMEM = {
	rules1_0, rules1_1, rules1_2, rules1_3, rules1_4, rules1_5, NULL,
};

                              // .........x.........x.XXX
const char rules2_0[] PROGMEM = "Bridges must be";
const char rules2_1[] PROGMEM = "created horizontally";
const char rules2_2[] PROGMEM = "or vertically only.";
const char rules2_3[] PROGMEM = "They must not cross";
const char rules2_4[] PROGMEM = "each other.";
const char rules2_5[] PROGMEM = "                (2/4)";
const char *const rules2[] PROGMEM = {
	rules2_0, rules2_1, rules2_2, rules2_3, rules2_4, rules2_5, NULL,
};

                              // .........x.........x.XXX
const char rules3_0[] PROGMEM = "Bridges can be single";
const char rules3_1[] PROGMEM = "(-) or double (=).";
const char rules3_2[] PROGMEM = "The sum of bridges";
const char rules3_3[] PROGMEM = "leaving an island";
const char rules3_4[] PROGMEM = "must match the number";
const char rules3_5[] PROGMEM = "of the island.  (3/4)";
const char *const rules3[] PROGMEM = {
	rules3_0, rules3_1, rules3_2, rules3_3, rules3_4, rules3_5, NULL,
};

                              // .........x.........x.XXX
const char rules4_0[] PROGMEM = "Finally, it must be";
const char rules4_1[] PROGMEM = "possible to reach any";
const char rules4_2[] PROGMEM = "island from any other";
const char rules4_3[] PROGMEM = "island, i.e. all";
const char rules4_4[] PROGMEM = "islands form a con-";
const char rules4_5[] PROGMEM = "nected group.   (4/4)";
const char *const rules4[] PROGMEM = {
	rules4_0, rules4_1, rules4_2, rules4_3, rules4_4, rules4_5, NULL,
};

const char** const PROGMEM rules[] = {
	rules1, rules2, rules3, rules4, NULL,
};

