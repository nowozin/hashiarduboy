#ifdef X86
#include <cstdint>

// Mock for PGM functionality on x86
uint8_t pgm_read_byte(const uint8_t* mem) { return *mem; };
#else
// Arduino
#include <Arduino.h>
#endif
#include <assert.h>

#include "hashi.h"
#include "bridgeenum.h"
#include "islandenum.h"

Hashi::Hashi(void) {
	reset();
}

island_t Hashi::board(uint8_t row, uint8_t col) const {
	return ((island_t) board_s[row][col] & BOARD_ISLAND_MASK);
}

bool Hashi::has_island_at(uint8_t row, uint8_t col) const {
	return board(row, col) > 0;
}

uint8_t Hashi::current_degree(uint8_t row, uint8_t col) const {
	return ((uint8_t) (
		(board_s[row][col] & BOARD_DEGREE_MASK)
			>> BOARD_DEGREE_SHIFT));
}
void Hashi::increment_degree(uint8_t row, uint8_t col, uint8_t value) {
	uint8_t current = current_degree(row, col);
	current += value;
	board_s[row][col] = (board_s[row][col] & ~BOARD_DEGREE_MASK)
		| ((current << BOARD_DEGREE_SHIFT) & BOARD_DEGREE_MASK);
}
void Hashi::set_degrees_to_zero(void) {
	// Set all degrees to zero
	IslandEnumerator isl = islands();
	while (isl.next()) {
		board_s[isl.row][isl.col] &= ~BOARD_DEGREE_MASK;
	}
}
void Hashi::recalculate_degrees(void) {
	set_degrees_to_zero();
	BridgeEnumerator bridge = bridges();
	while (bridge.next()) {
		uint8_t value = state[bridge.bi];
		increment_degree(bridge.r1, bridge.c1, value);
		increment_degree(bridge.r2, bridge.c2, value);
	}
}

void Hashi::reset(void) {
	for (uint8_t row = 0; row < HASHI_MAX_HEIGHT; ++row)
		for (uint8_t col = 0; col < HASHI_MAX_WIDTH; ++col)
			board_s[row][col] = 0;

	for (uint8_t ei = 0; ei < HASHI_MAX_BRIDGES; ++ei) {
		state[ei] = 0;
		solution[ei] = 0;
	}
}

// Return true if candidate is a valid solution to current puzzle.
bool Hashi::is_solution(void) {
	return is_solution_degree() && is_solution_connected();
}

// Return true if candidate is a valid solution to current puzzle, degree-check
// only (not connectivity).
bool Hashi::is_solution_degree(void) const {
	// First check all current degrees
	IslandEnumerator island = islands();
	while (island.next()) {
		if (current_degree(island.row, island.col) !=
			board(island.row, island.col))
			return false;
	}
	return true;
}

bool Hashi::is_solution_connected(void) {
	// Simple connectivity algorithm, O(I B) complexity:
	//   1. Tag one island as positive, all others as negative.
	//   2. Repeat I times:
	//   	a) For each bridge b
	//   	b) If one bridge node is positive, mark the other as positive.
	//   3. When there are no more changes: check that all islands are
	//      positive.

	// We use degrees as positive marker.
	set_degrees_to_zero();

	// Tag the first island as positive
	IslandEnumerator island = islands();
	bool found_first = island.next();
	assert(found_first);
	assert(current_degree(island.row, island.col) == 0);
	increment_degree(island.row, island.col, 1);

	// Repeat label propagation
	uint8_t island_count = count_islands();
	bool update_in_prev_iter = true;
	for (uint8_t iteration = 0;
		update_in_prev_iter && iteration < island_count;
		++island_count)
	{
		update_in_prev_iter = false;

		BridgeEnumerator bridge = bridges();
		while (bridge.next()) {
			// Bridge not part of solution: skip
			if (state[bridge.bi] == 0)
				continue;

			if (
				current_degree(bridge.r1, bridge.c1) > 0
				&& current_degree(bridge.r2, bridge.c2) == 0)
			{
				// positive --- negative
				increment_degree(bridge.r2, bridge.c2, 1);
				update_in_prev_iter = true;
			} else if (
				current_degree(bridge.r1, bridge.c1) == 0
				&& current_degree(bridge.r2, bridge.c2) > 0)
			{
				// negative --- positive
				increment_degree(bridge.r1, bridge.c1, 1);
				update_in_prev_iter = true;
			}
		}
	}
	// Check that all nodes are marked
	bool is_connected = true;
	island.reset();
	while (island.next()) {
		if (current_degree(island.row, island.col) > 0)
			continue;

		is_connected = false;
		break;
	}

	// Restore degrees
	recalculate_degrees();

	return is_connected;
}

uint8_t Hashi::label_connected_components(solution_t* comp) const {
	// Simple component labeling algorithm, O(I B) complexity:
	//   1. Label each island from 1 to I, consecutively.
	//   2. Repeat I times:
	//   	a) For each bridge b that is active,
	//   	b) If both islands have different label, propagate the smaller.
	//   3. When there are no more changes: stop
	//   4. Compact labels by sequentially re-labeling islands.
	uint8_t labels[HASHI_MAX_ISLANDS];
	uint8_t island_count = count_islands();
	IslandEnumerator island = islands();
	for (uint8_t label = 1;
		island.next() && label <= island_count;
		++label)
	{
		labels[island.index] = label;
	}

	// Main propagation loop
	bool update_in_prev_iter = true;
	for (uint8_t iteration = 0;
		update_in_prev_iter && iteration < island_count;
		++island_count)
	{
		update_in_prev_iter = false;

		BridgeEnumerator bridge = bridges();
		while (bridge.next()) {
			// Bridge not part of solution: skip
			if (state[bridge.bi] == 0)
				continue;

			uint8_t index1 = bridge.index1();
			uint8_t label1 = labels[index1];
			uint8_t index2 = bridge.index2();
			uint8_t label2 = labels[index2];
			if (label1 == label2)
				continue;	// already the same

			update_in_prev_iter = true;
			if (label1 < label2) {
				// Preserve label1, the smaller label
				labels[index2] = label1;
			} else {
				// Preserve label2, the smaller label
				labels[index1] = label2;
			}
		}
	}

	// Compact labels:
	//    1. Create auxiliary array of new labels;
	//    2. Make one sequential pass over all islands to relabel.
	uint8_t component_count = 0;
	uint8_t labels_new[HASHI_MAX_ISLANDS];
	for (uint8_t ii = 0; ii < HASHI_MAX_ISLANDS; ++ii)
		labels_new[ii] = 0;

	island.reset();
	while (island.next()) {
		uint8_t current_label = labels[island.index];
		if (labels_new[current_label] == 0) {
			// Assign new label
			labels_new[current_label] = component_count + 1;
			component_count += 1;
		}
		// Label exists now, relabel
		labels[island.index] = labels_new[current_label];
	}

	// Create bridge-labeling
	BridgeEnumerator bridge = bridges();
	while (bridge.next()) {
		uint8_t bi = bridge.bi;

		// Bridge not part of solution: skip
		if (state[bi] == 0) {
			comp[bi] = 0;
			continue;
		}

		uint8_t index1 = bridge.index1();
		uint8_t label1 = labels[index1];
		uint8_t index2 = bridge.index2();
		uint8_t label2 = labels[index2];
		assert(label1 == label2);

		comp[bi] = label1;
	}
	return component_count;
}

BridgeEnumerator Hashi::bridges(void) const {
	return BridgeEnumerator(this);
}
BridgeEnumerator Hashi::bridge(uint8_t bi) const {
	BridgeEnumerator ee = bridges();
	while (ee.next() && ee.bi < bi)
		;
	return ee;
}

IslandEnumerator Hashi::islands(void) const {
	return IslandEnumerator(this);
}

uint8_t Hashi::count_islands(void) const {
	IslandEnumerator island = islands();
	uint8_t count = 0;
	while (island.next())
		count += 1;

	return count;
}

int16_t Hashi::find_bridge_at(
  uint8_t row, uint8_t col, bool horizontal) const
{
	BridgeEnumerator ee = bridges();

	for (int16_t bi = 0; ee.next(); ++bi) {
		if (ee.is_horizontal() == horizontal && ee.contains(row, col))
			return bi;
	}
	return -1;
}

int16_t Hashi::find_bridge_at_island(
	uint8_t row, uint8_t col, BridgeDirection direction) const
{
	if (has_island_at(row, col) == false)
		return -1;

	BridgeEnumerator ee = bridges();

	for (int16_t bi = 0; ee.next(); ++bi) {
		if (!ee.ends_at(row, col))
			continue;

		// Bridge is connected to island, check direction
		switch (direction) {
		case BDIR_UP:
			// (row,col) must be (r2,c2)
			if (ee.is_horizontal() == false
				&& ee.r2 == row
				&& ee.c2 == col)
				return ee.bi;
			break;
		case BDIR_RIGHT:
			// (row,col) must be (r1,c1)
			if (ee.is_horizontal()
				&& ee.r1 == row
				&& ee.c1 == col)
				return ee.bi;
			break;
		case BDIR_DOWN:
			// (row,col) must be (r1,c1)
			if (ee.is_horizontal() == false
				&& ee.r1 == row
				&& ee.c1 == col)
				return ee.bi;
			break;
		case BDIR_LEFT:
			// (row,col) must be (r2,c2)
			if (ee.is_horizontal()
				&& ee.r2 == row
				&& ee.c2 == col)
				return ee.bi;
			break;
		default:
			break;
		}
	}
	return -1;
}

uint8_t Hashi::count_bridges(void) const {
	BridgeEnumerator ee(this);
	uint8_t bridge_count = 0;
	while (ee.next())
		bridge_count += 1;
	return bridge_count;
}

#if 0
// Old and correct implementation of counting bridges
uint8_t Hashi::count_bridges(void) const {
	uint8_t bridge_count = 0;
	for (uint8_t r1 = 0; r1 < height; ++r1) {
		for (uint8_t c1 = 0; c1 < width; ++c1) {
			if (board[r1][c1] == 0)
				continue;

			// Check same row
			for (uint8_t c2 = c1+1; c2 < width; ++c2) {
				bool has_value = board[r1][c2] > 0;
				if (has_value && c2 == c1+1)
					break;	// adjacent: V V

				if (has_value) {
					bridge_count += 1;
					break;
				}
			}

			// Check same column
			for (uint8_t r2 = r1+1; r2 < height; ++r2) {
				bool has_value = board[r2][c1] > 0;
				if (has_value && r2 == r1+1)
					break;	// adjacent: V / V

				if (has_value) {
					bridge_count += 1;
					break;
				}
			}
		}
	}
	return bridge_count;
}
#endif

// Decode bit-compressed solution from PGM memory into solution member.
// See `ENCODING.md` for the description of the encoding used.
void Hashi::decode_solution_from_pgm_memory(
	const uint8_t* sol_start, uint8_t bridge_count)
{
	const uint8_t* sol = sol_start;
	uint8_t ei = 0;

	uint8_t byte = 0x00;
	while (bridge_count > 0) {
		byte = pgm_read_byte(sol);
		sol += 1;

		if (bridge_count >= 4) {
			// 0011.2233
			solution[ei+0] = (byte >> 6) & 0x03;
			solution[ei+1] = (byte >> 4) & 0x03;
			solution[ei+2] = (byte >> 2) & 0x03;
			solution[ei+3] = byte & 0x03;
			bridge_count -= 4;
			ei += 4;
		} else if (bridge_count == 3) {
			// xx00.11.22
			solution[ei+0] = (byte >> 4) & 0x03;
			solution[ei+1] = (byte >> 2) & 0x03;
			solution[ei+2] = byte & 0x03;
			ei += 3;
			bridge_count = 0;
		} else if (bridge_count == 2) {
			// xxxx.0011
			solution[ei+0] = (byte >> 2) & 0x03;
			solution[ei+1] = byte & 0x03;
			ei += 2;
			bridge_count = 0;
		} else if (bridge_count == 1) {
			// xxxx.xx00
			solution[ei+0] = byte & 0x03;
			ei += 1;
			bridge_count = 0;
		}
	}
}

// Instantiate a Hashi board from a ROM-stored encoding.
// See `ENCODING.md` for a description of the encoding used.
void Hashi::decode_from_pgm_memory(const uint8_t* mem) {
	reset();

	height = pgm_read_byte(&mem[0]);
	assert(3 <= height);
	assert(height <= HASHI_MAX_HEIGHT);
	width = pgm_read_byte(&mem[1]);
	assert(3 <= width);
	assert(width <= HASHI_MAX_WIDTH);

	// Decode island list
	uint8_t island_count = pgm_read_byte(&mem[2]);
	assert(2 <= island_count);
	const uint8_t* island_start = &mem[4];
	for (uint8_t i = 0; i < island_count; ++i) {
		uint8_t rowcol = pgm_read_byte(&island_start[(i << 1)]);
		uint8_t row = (rowcol >> 4) & 0x0f;
		assert(row <= HASHI_MAX_HEIGHT);
		uint8_t col = rowcol & 0x0f;
		assert(col<= HASHI_MAX_WIDTH);
		uint8_t value = pgm_read_byte(&island_start[(i << 1) + 1]);
		assert(value <= 8);
		board_s[row][col] = value;
	}

	// Infer edges in correct order
	uint16_t bridge_count_from_board = count_bridges();

	// Decode solution
	uint8_t bridge_count = pgm_read_byte(&mem[3]);
	assert(bridge_count == 0 || bridge_count == bridge_count_from_board);
	if (bridge_count > 0) {
		const uint8_t* sol_start = &island_start[island_count << 1];
		decode_solution_from_pgm_memory(sol_start, bridge_count);
	}
}
