#ifndef	HASHI_H
#define	HASHI_H

#include "bridgeenum.h"
#include "islandenum.h"

#define	HASHI_MAX_WIDTH 16
#define	HASHI_MAX_HEIGHT 8
#define HASHI_MAX_BRIDGES 128
#define HASHI_MAX_ISLANDS 64

#define	BOARD_ISLAND_MASK 0x0f
#define	BOARD_DEGREE_MASK 0xf0
#define	BOARD_DEGREE_SHIFT 4

typedef uint8_t	island_t;
typedef uint8_t	solution_t;

enum BridgeDirection {
	BDIR_UP = 0,
	BDIR_RIGHT = 1,
	BDIR_DOWN = 2,
	BDIR_LEFT = 3,
};

class Hashi {
public:
	uint8_t	height;
	uint8_t	width;

	// board[row][col]
	island_t board_s[HASHI_MAX_HEIGHT][HASHI_MAX_WIDTH];
	solution_t state[HASHI_MAX_BRIDGES];
	solution_t solution[HASHI_MAX_BRIDGES];

	// Return the target value of the island at the given coordinates.
	island_t board(uint8_t row, uint8_t col) const;
	bool has_island_at(uint8_t row, uint8_t col) const;

	// Return the current degree at the given island coordinates, wrt
	// the `state` candidate solution.  Whenever `state` has been modified,
	// `recalculate_degrees` must be called first.
	uint8_t current_degree(uint8_t row, uint8_t col) const;
	// From `state`, compute the active degrees at each island.
	void set_degrees_to_zero(void);
	void recalculate_degrees(void);
	void increment_degree(uint8_t row, uint8_t col, uint8_t value);

	void reset(void);
	Hashi(void);

	// Checking `state` as solution: degree-satisfaction and connectivity
	bool is_solution(void);
	// Check if `state` represents a degree-correct solution.
	bool is_solution_degree(void) const;
	// Check if `state` represents a connected graph, independent of degree
	// satisfaction.
	bool is_solution_connected(void);

	// Label each bridge as part of a connected component.
	// Bridges that are not selected have value 0, every other
	// edge has a connected component label 1 <= label <= K, where
	// K is the number of connected components.
	// We must have `solution_t comp[HASHI_MAX_BRIDGES]`, and the method
	// returns K, the number of connected components.
	uint8_t label_connected_components(solution_t* comp) const;

	BridgeEnumerator bridges(void) const;
	// Skip directly to given bridge index `bi`.
	BridgeEnumerator bridge(uint8_t bi) const;
	IslandEnumerator islands(void) const;
	uint8_t count_islands(void) const;
	// Find horizontal or vertical bridge at non-island coordinates
	// (row, col).
	// Return -1 if no horizontal/vertical bridge exists at (row,col).
	// Return bridge index if a horizontal/vertical bridge exists at (row,col).
	int16_t find_bridge_at(uint8_t row, uint8_t col, bool horizontal) const;
	// Find bridge at given island coordiante (row, col) in the given direction.
	// Return -1 if no such bridge exists.
	// Return bridge index if a bridge in the direction exists.
	int16_t find_bridge_at_island(uint8_t row, uint8_t col,
		BridgeDirection direction) const;
	uint8_t count_bridges(void) const;

	void decode_solution_from_pgm_memory(
		const uint8_t* sol_start, uint8_t bridge_count);
	void decode_from_pgm_memory(const uint8_t* mem);
};

#endif
