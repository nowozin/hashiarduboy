#ifndef ISLANDENUM_H
#define ISLANDENUM_H

#ifdef X86
#include <cstdint>
#else
#include <Arduino.h>
#endif

class Hashi;

enum IslandEnumState {
	ISLAND_NEW_CELL = 0,
	ISLAND_FOUND_ISLAND = 1,
	ISLAND_END_REACHED = 2,
};

class IslandEnumerator {
private:
	const Hashi* hashi;

	IslandEnumState state;

	// Increment (r1,c1) coordinates.
	// Return true if end is reached.
	bool increment(void);

public:
	uint8_t	index;	// island index
	uint8_t	row;	// island coordinate, row
	uint8_t col;	// island coordinate, column

	// Return true if has found a new bridge
	bool next(void);
	void reset(void);

	IslandEnumerator(const Hashi* hashi);
};

#endif


