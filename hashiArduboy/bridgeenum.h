#ifndef BRIDGEENUM_H
#define BRIDGEENUM_H

#ifdef X86
#include <cstdint>
#else
#include <Arduino.h>
#endif

class Hashi;

enum BridgeEnumState {
	NEW_CELL = 0,
	FOUND_HORIZONTAL = 1,
	FOUND_VERTICAL = 2,
	END_REACHED = 3,
};

class BridgeEnumerator {
private:
	const Hashi* hashi;

	BridgeEnumState state;

	// If returns false: (r2,c2) to be ignored.
	// If returns true: (r2,c2) contain the bridge end.
	bool find_horizontal(void);
	bool find_vertical(void);

	// Increment (r1,c1) coordinates.
	// Return true if end is reached.
	bool increment(void);

	// Return absolute island index of island at (row, col)
	uint8_t index(uint8_t row, uint8_t col) const;

public:
	uint8_t	bi;	// bridge index
	uint8_t r1;
	uint8_t c1;
	uint8_t r2;
	uint8_t c2;

	// Return true if has found a new bridge
	bool next(void);
	void reset(void);

	// Return the absolute island index of first/second bridge endpoint
	uint8_t index1(void) const;
	uint8_t index2(void) const;

	// Return true if current bridge is horizontal, false if vertical.
	bool is_horizontal(void) const;

	// Returns true if the current bridge is crossing (row,col).
	bool contains(uint8_t row, uint8_t col) const;

	// Returns true if the current bridge has one endpoint at island
	// (row,col).
	bool ends_at(uint8_t row, uint8_t col) const;

	// Return true if current bridge in `ee` intersects with current edge
	// in `this`.
	bool intersects_with(const BridgeEnumerator& ee) const;

	BridgeEnumerator(const Hashi* hashi);
};

#endif

