
#include "bridgeenum.h"
#include "islandenum.h"
#include "hashi.h"

#include <assert.h>


// If returns false: (r2,c2) to be ignored.
// If returns true: (r2,c2) contain the bridge end.
bool BridgeEnumerator::find_horizontal(void) {
	// (r1,c1) has degree > 0, find horizontal
	r2 = r1;
	for (c2 = c1+1; c2 < hashi->width; ++c2) {
		bool has_island = hashi->has_island_at(r1, c2);
		if (has_island && c2 == c1+1)
			return false;	// adjacent: V V

		if (has_island)
			return true;	// found; (r2,c2) are up-to-date
	}
	return false;
}
bool BridgeEnumerator::find_vertical(void) {
	// (r1,c1) has degree > 0, find vertical
	c2 = c1;
	for (r2 = r1+1; r2 < hashi->height; ++r2) {
		bool has_island = hashi->has_island_at(r2, c1);
		if (has_island && r2 == r1+1)
			return false;	// adjacent: V / V

		if (has_island)	// found; (r2,c2) are up-to-date
			return true;
	}
	return false;
}

// Increment (r1,c1) coordinates.
// Return true if end is reached.
bool BridgeEnumerator::increment(void) {
	c1 += 1;
	if (c1 >= hashi->width) {
		r1 += 1;
		c1 = 0;
	}
	return (r1 >= hashi->height);
}

// Return true if has found a new bridge
bool BridgeEnumerator::next(void) {
	assert(state != END_REACHED);

	// Possible state transitions:
	//
	//              .--------------------.
	//              |                    |
	//              v                    |
	//     .---- NEW_CELL ---> FOUND_HORIZONTAL (emit)
	//     |        ^  \                 |
	//     v        |   \                v
	// END_REACHED  |    '----> FOUND_VERTICAL (emit) ---.
	//              |                                    |
	//              '------------------------------------'
	while (true) {
		switch (state) {
		case NEW_CELL:
			while (!hashi->has_island_at(r1, c1)) {
				if (increment()) {
					state = END_REACHED;
					return false;
				}
			}
			// Found a non-empty cell
			// (r1, c1) has degree > 0, find horizontal
			if (find_horizontal()) {
				state = FOUND_HORIZONTAL;
				return true;
			}
			// FALLTHROUGH
		case FOUND_HORIZONTAL:
			// If fallthrough: do not increment index
			if (state == FOUND_HORIZONTAL)
				bi += 1;

			// (r1, c1) has degree > 0, find vertical
			if (find_vertical()) {
				state = FOUND_VERTICAL;
				return true;
			}
			state = NEW_CELL;
			break;
		case FOUND_VERTICAL:
			bi += 1;
			state = NEW_CELL;
			break;
		}

		// Found neither
		if (increment()) {
			state = END_REACHED;
			return false;
		}
	}
}

void BridgeEnumerator::reset(void) {
	bi = 0;
	r1 = 0;
	c1 = 0;
	r2 = 0;
	c2 = 0;
	state = NEW_CELL;
}

uint8_t BridgeEnumerator::index(uint8_t row, uint8_t col) const {
	IslandEnumerator island = hashi->islands();
	while (island.next()) {
		if (island.row == row && island.col == col)
			return island.index;
	}
	assert(0);

	return 0;
}
uint8_t BridgeEnumerator::index1(void) const {
	return index(r1, c1);
}
uint8_t BridgeEnumerator::index2(void) const {
	return index(r2, c2);
}

bool BridgeEnumerator::is_horizontal(void) const {
	return (r1 == r2);
}

bool BridgeEnumerator::contains(uint8_t row, uint8_t col) const {
	if (is_horizontal()) {
		// Horizontal: (r1,c1) ---- (r2==r1,c2)
		return (row == r1 && col > c1 && col < c2);
	} else {
		// Vertical:  (r1,c1)
		//               |
		//            (r2,c2==c1)
		return (col == c1 && row > r1 && row < r2);
	}
}

bool BridgeEnumerator::ends_at(uint8_t row, uint8_t col) const {
	return (row == r1 && col == c1) || (row == r2 && col == c2);
}

bool BridgeEnumerator::intersects_with(const BridgeEnumerator& ee) const {
	// Parallel bridges, i.e. horizontal/horizontal and vertical/vertical
	// pairs cannot intersect with each other.
	if (is_horizontal() == ee.is_horizontal())
		return false;

	if (is_horizontal() && ee.is_horizontal() == false) {
		// `this` is horizontal, `ee` is vertical
		//
		//          (ee.r1,ee.c1)
		//                |
		//  (r1,c1) ------+------ (r2,c2)
		//                |
		//          (ee.r2,ee.c2)
		if (ee.c1 > c1 && ee.c1 < c2 && ee.r1 < r1 && ee.r2 > r1)
			return true;
	} else if (is_horizontal() == false && ee.is_horizontal()) {
		// `ee` is horizontal, `this` is vertical
		//
		//                   (r1,c1)
		//                      |
		//  (ee.r1,ee.c1) ------+------ (ee.r2,ee.c2)
		//                      |
		//                   (r2,c2)
		if (ee.r1 > r1 && ee.r1 < r2 && ee.c1 < c1 && ee.c2 > c1)
			return true;
	}
	return false;
}

BridgeEnumerator::BridgeEnumerator(const Hashi* hashi)
	: hashi(hashi)
{
	reset();
}

