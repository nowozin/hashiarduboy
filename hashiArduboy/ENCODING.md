
# PGM Hashi Encoding

A _Hashi instance_ is stored in ROM in a compact encoded format.
The encoding consists of a header, followed by an island list, followed
optionally by a solution.

## Header encoding

* `height`: `uint8_t`, the height of the Hashi instance, typically 8.
  Must have `3 <= height <= 8`.
* `width`: `uint8_t`, the width of the Hashi instance, typically 8, 12, or 16.
  Must have `3 <= width <= 16`.
* `island_count`: `uint8_t`, the number of islands present on the board.
  Must have `2 <= island_count`.
* `bridge_count`: `uint8_t`, the number of possible bridges induced by the
  islands.  If this is equal to `0x00`, then no solution is encoded.


## Island list

The island list immediately followed the header and consists of `island_count`
items, each of the following format:

* `rowcol`: `uint8_t`, with bit pattern `rrrr.cccc`, where `rrrr` contains the
  row index from `0` to `7` and `cccc` contains the column index from `0` to
  `15`.
* `value`: `uint8_t`, value of the island.  Must have `0 < value <= 8`.


## Solution encoding

If `bridge_count > 0` in the header, we also encode the solution.  All possible
bridges are enumerated in a specific total order as implemented in `hashi.h`.
For this ordering we store the bridge value, which can be 0, 1, or 2 and are
encoded using two bits `bb`.  Each bridge value is encoded as two bits in the
following ordering.

```
0011.2233 4455.6677 ...
```

If `bridge_count % 8 != 0`, then a final byte-boundary is enforced.  For
example, if there are 6 bridges, then the encoding would be:

```
0011.2233 xxxx.4455
```

Here `xxxx` are left unused.

