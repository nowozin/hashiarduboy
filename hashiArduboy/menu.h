#ifndef MENU_H
#define MENU_H

#include <Arduboy2.h>

struct Menu {
	// Position to paint the menu
	uint8_t	draw_pos_x;
	uint8_t	draw_pos_y;

	uint8_t	item_count;
	uint8_t	current_selection;

	const char** item_text;
};

enum MenuResult {
	IN_MENU = 0,
	SELECTION_MADE = 1,
	BACK = 2,
};

// Handle active menu:
//   1. handle buttons (`pollButtons` needs to be called prior to `handleMenu`).
//   2. draw menu on screen.
//   3. return result:
//      - IN_MENU: nothing happened, user is still choosing;
//      - SELECTION_MADE: The selected value is available through
//        `menu->current_selection`;
//      - BACK: user pressed the B button for going back.
MenuResult handleMenu(Arduboy2* arduboy, Menu* menu);

#endif

